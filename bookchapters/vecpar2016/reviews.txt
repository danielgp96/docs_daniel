----------------------- REVIEW 1 ---------------------
PAPER: 1
TITLE: Versat, a Minimal Coarse-Grain Reconfigurable Array
AUTHORS: Joao Lopes and Jose Sousa

OVERALL EVALUATION: 2 (accept)

----------- Review -----------
Well written, very interesting to read paper, and certainly a hot topic of research. My general comments on the paper are:


- In the introduction, sentences started with “It has been
  demonstrated several times that certain algorithms can run orders of
  magnitude faster and consume [..]” and “Examples of target kernels
  are [..]” do need to be backed up by convenient references.

- What do you mean in introduction with “In this work we exploit the
  similarity of different CGRA configurations by using partial
  reconfiguration”? The comments following this sentence mention the
  disadvantage of using full reconfiguration, which I guess are clear
  to everybody. But then I don’t find information in the paper that
  shows the gains achieved in terms of reconfiguration time, which
  appear to be the main problems of [5] and [6]. Or does your strategy
  only consider energy consumption savings? In other words, you should
  have quantified how much your system performs better against [5] and
  [6] also in terms of time. Please clarify this.

- What does “cdp” stand for in table 2? What is a complex dot product?

- Does the energy ratio in Table 2 mean that values >1 indicate that
  your solution using ARM+Versat is more efficient than the single ARM
  alone?

- In section 3 when you say “In fact only 638 cycles are unhidden
  [..]” for which case are you referring to? The fft? Please clarify.

- The last claim in the last paragraph of section 3 isn’t quite exact,
  is it? I mean, you should have scaled down the Morphosys [2]
  technology from 350nm to a 40 nm process design as you did with your
  design, right? Can you please comment that in the text?

- Finally, I feel curious about the way you control the
  generation/compilation of opcode starting from OpenCL kernels, since
  your architecture only supports 16 instructions. Do you somehow
  manipulate the generated Assembly code? I understand this is subject
  for a distinct paper, but some light here would be nice…

Typos:

Please replace “run time” by “runtime”  throughout the text.

Please correct “That is Versat is 10x smaller [..]”


----------------------- REVIEW 2 ---------------------
PAPER: 1
TITLE: Versat, a Minimal Coarse-Grain Reconfigurable Array
AUTHORS: Joao Lopes and Jose Sousa

OVERALL EVALUATION: -1 (weak reject)

----------- Review ----------- The paper presents a coarse-grain
reconfigurable array called Versat.  The innovation with Versat
compared with other CGRAs is that it allows partial reconfiguration
efficiently, and that it has only a small number of functional units
combined with a simple controller.

On the whole, the paper was interesting, but kept the description of
Versat to such a high level it was impossible to evaluate how
realistic it is.  The diagrams provided are useful (figures 1, 2, and
3) but never fully explain how it actually works.  For example, how
are the functional units actually configured?  There is space for a
number of configurations (15), but how do these alter the functional
units' logical organization when they are used?  It is very opaque
from the description.

A diagram here would really help.  The paper mentions Versat being
able to exploit ILP, TLP, and DLP and it would be beneficial to see
examples of each of these.

Further, when talking about the controller, an example configuration
program would help, even if it were in pseudo-code, just to show how
the programs look and what features of C++ can be used.

Another criticism is that Versat is never qualitatively compared to any other CGRA.  What design decisions were taken to develop Versat that were different to other schemes; why were the authors' choices more optimal?

Results seem a little hand-wavy.  Each of the 4 architectures has
numbers from a different technology node and then scaling factors used
to convert to a common form.  Of course, you'd expect Versat to be
better than the ARM processor for small kernels in terms of
performance.  What about being better than any of the other CGRAs?
(Yes, you use numbers from a published paper, but what about
implementing it in your setup for a fair comparison?)  In addition,
how does Versat perform when there's a real application being run and
it has to be configured (and reconfigured) periodically to different
kernels that the application uses?

Finally, what do the 43 different configurations for FFT actually look
like?  How similar are they?  This would be interesting and give some
intuition about the way Versat works and what can be mapped onto it.


----------------------- REVIEW 3 ---------------------
PAPER: 1
TITLE: Versat, a Minimal Coarse-Grain Reconfigurable Array
AUTHORS: Joao Lopes and Jose Sousa

OVERALL EVALUATION: 1 (weak accept)

----------- Review ----------- In this paper the authors present
Versat a coarse-grain reconfigurable array.

The authors correctly present the motivation for
coarse-reconfiguration hardware accelerators. The architecture seems
sound. The work is relevant.

The comparison with other architectures in Table 1 seems a bit
confusing. I believe the authors need to re-check the values they
present in the text from the scaling as there seem to be slight
differences from what should be the correct values. The scaled power
value is closer to 10x instead of 15x like it is mentioned (do not
forget that the ARM is running at 800MHz not 600MHz).

My criticism for the work is in the evaluation. The authors present
results for the implementation of simple operations that map directly
in the proposed architecture. It is not clear what the impact of this
is in the execution of the whole application. In addition, it is not
clear if all overheads are accounted for (data transfers,
reconfiguration,...). Also, it would be interesting to add the
comparison to a fine-grain FPGA reconfigurable hardware for the
acceleration of the same code. Finally, it is not clear if the
coarse-grain reconfiguration means that there is a fixed set of
operations available in the accelerator. If so then which operations
and why? This is also not clear from the text.

Overall, while the work does not bring any great novelty, it seems to
be sound and it is relevant and interesting. Its evaluation is still
very immature and thus the conclusions can not be as general as
claimed.


----------------------- REVIEW 4 ---------------------
PAPER: 1
TITLE: Versat, a Minimal Coarse-Grain Reconfigurable Array
AUTHORS: Joao Lopes and Jose Sousa

OVERALL EVALUATION: 2 (accept)

----------- Review ----------- The paper presents a Coarse-Grain
Reconfigurable Array (CGRA) with support for partial reconfiguration,
and a programmable control unit. This allows for using a smaller array
than other CGRAs.

Table 2 and the last two paragraphs of Section 3 present the
results. Adding a bit more of information would be valuable. For
example, the text indicates the cycles for data transfer and for
control, but not for all kernels. I believe it would be good to
include this for all kernels in the table.  Only one of the kernels
requires partial reconfiguration, and that in this case,
reconfiguration time can be overlapped. These results are excellent,
but I consider that, being partial reconfiguration the most important
feature of Versat (if I understood correctly the paper), the
evaluation should include mostly kernels that require partial
reconfiguration, to fully understand the benefits and limitations of
the approach. This doesn't influence my decision, since I believe that
for workshop papers, the idea is far more important than the
evaluation, but I suggest the authors to strengthen this part before
submitting to a conference.

Hovewer, I would like to see more detailed information about the
reconfigurations of the kernels included. In the kernels that do not
require partial reconfiguration, how many FUs are used? If you had
less than 15 FUs, would the results have changed much? In the partial
reconfigurations of FFT, how much (%) of the CGRA is reconfigured
every time? It is close to 100%, it would be possible to design a
system without partial reconfiguration if it allows overlapping
computation and reconfiguration.

One part that was not very clear is the description of the Controller
in Section 2.3. You indicate "Versat uses of a minimal controller for
reconfiguration, data transfer and simple algorithmic control." Can
you expand more on this? Which type of algorithmic control? Just to
count the number of iterations or something more advanced (allowing
if-then-else constructions, for example)? Is there anything in the
Controller necessary for allowing to overlap computation and
reconfiguration, or it is simply the result of having multiple
configurations in a memory + the configuration register?

You should reference the work on CGRAs by the University of Wisconsin
(http://pages.cs.wisc.edu/~karu/wiki/). The DySER papers are more
recent that the ones you cite, and I consider this work is relevant
for your research.

Overall, I consider this paper is interesting for the workshop.


----------------------- REVIEW 5 ---------------------
PAPER: 1
TITLE: Versat, a Minimal Coarse-Grain Reconfigurable Array
AUTHORS: Joao Lopes and Jose Sousa

OVERALL EVALUATION: 1 (weak accept)

----------- Review -----------
Summary
=======

The Paper describes a CGRA architecture with a limited number of
functional units that are intended to be frequently reconfigured. Fast
reconfiguration can happen on each different loop.  Compared with an
ARM9 the architecture can be up to 17 times more performant and energy
efficient.

Bad === In the introduction you state that CGRA have demonstrated
several times that they consume less power, yet you do not provide any
reference in that sentence. Maybe this is explained in other
references that you introduce later, but then the context is lost.

I do not understand why not llvm or gcc wouldn't be powerful enough to
describe the datapaths of the CGRA architecture. Some more details of
the reasons for that should be needed from my point of view.

When comparing power consumption in Table 1, besides stating that the
numbers where obtained by Cadence tools, you should describre the
conditions that were assumed to get those numbers. Either a certain
generic switching activity rate (as a %) or a certain activity derived
from running a benchmark.

Moreover, I argue that the energy efficiency (Ops/Watt) is more
important that power itself.

You present that number in table 2. But, as it is executed on Zynq,
then it is not clear if you are also considering the static power
consumption of the FPGA when running only the ARM benchmark.

Good
====

The paper is well structured and it's easyly readable. It does a good
job describing similar works and their differences.
