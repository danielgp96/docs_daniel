The authors would like to thank the very useful comments and suggestions for
improvement of the reviewers. The reviewers comments are reproduce below and the
authors' response are inserted inline preceded by "R:"


****************************************
Reviewer #1 Comments
****************************************

The paper is well written and well organized, and the idea is explained
obviously.

Minor comments: 

- Figure 5 is inserted before it is cited in the text.

R: The figure is cited in the text so the reader can know which figure the author
is referring to. Figures are floating bodies and the text processing tool
chooses where it is best to inserted insert the figure. No action seems to be necessary.

- in page 4, first col., the last sentence should be edited to "The FPGA implementation results for ARRIA V and KINTEX-7 are shown in Tables I and II, respectively.".

R: Done. It is Tables II and III, respectively.

****************************************
Reviewer #2 Comments
****************************************

The authors design and implement a low power multicore architecture suitable for
fpga and asic applications.  The solution uses reconfiguration techniques to fit
larger systems on meduim FPGA and be suitable for ASIC as well.  The authors
report their implementations power, are and frequency. However, there is no test
on a specific application seeing how it compares to tother low-power systems
existing in the litterature except to ARM cores.  A comparison to certain
referenced papers in the paper references could give the work at hand higher
importance.


R: Comparisons to other CGRAs have been made in previous publications and are
not the main purpose of these paper. CGRAs are not as commonly used as ARM-based
platforms and the authors believe that this is caused by the lack of evidence on
their advantages. Therefore, in this paper it was chosen to present this
evidence in order to advance the knowledge in this subject. Competition with
other CGRAs is not the main thrust of the paper. The proposed platform is not
the only CGRA platform and the performance of a CGRA platform depends on the
resources deployed. The main thrust of the paper is to demonstrate to the
academic and industrial communities that using CGRAs, regardless if it is the
proposed or another platform, is a valid alternative to the incumbent
processor-based systems and should be construed as an endorsement of all
research efforts that promote the use of CGRAs.

In order to better convey this idea the following sentences were included in
Section IV-B:

"The present platform is compared with an ARM Cortex-
A9 processor running the same applications. Unfortunately, it
was not possible to compare with other CGRAs [3] because
they were not available to be programmed with the same
applications. Comparisons using published results were made
in [5] and it was concluded that they depend on the resources
of the CGRAs being compared."


****************************************
Reviewer #3 Comments
****************************************
I thank the authors for their interesting manuscript and have some comments:

Page 2, column 2, just before subsection B.
"of the in Fig. 3" should be "in Fig. 3"

R: Fixed.

Page 3, column 2,
"(c) is involves" should be "(c) involves"

R: Fixed.


Page 5:
"  Results have been extrapolated (not measured) to 28nm and
40nm nodes to illustrate how they vary with the technology"
In my opinion, this extrapolation is not useful as it depends on the rules
of reference 20 which has its own assumptions. I suggest you either implement
your design in 40nm and 28nm or remove the extrapolation.

R: The extrapolations have been eliminated as suggested as only the results for
the 65 and 130 nm nodes are presented. However, when comparing to the ARM-based
system the normalization to 40 nm has been kept to make it more convenient for
the reader to compare the implementations.


Page 5 and 6:
Table IX and the corresponding text do not discuss the reasons that the RAM
for your design is much larger than the case of ARM. Also what is the
execution time of the program in both cases and what is the total energy
consumed. My guess is that it took less time on ARM versus your proposal,
does the time difference compensate for the higher power of ARM so that at the
end the ARM solution has lower energy? Please clarify.

R: This is a very good point. Given the space available in the paper it is
difficult to include a cabal explanation. Even so, we improved the paragraph
indicating that 

"The ARM system needs a lower operating frequency and less
memory. However, the CGRA system uses much less logic
resources because CGRAs are data-centric and do not process
instructions. Being data-centric also implies the use of more
memory."

Page 6:
The references should use a consistent style. Currently some references
use the full names of the authors while others use the first initial
followed by last name.

R: The references' style has been made consistent. 
