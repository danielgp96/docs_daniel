\documentclass[conference,a4paper]{IEEEtran}
\usepackage{amsmath}
\usepackage[pdftex]{graphicx}
% declare the path(s) where your graphic files are
\graphicspath{{./drawings/}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpeg,.png,.eps}

% correct bad hyphenation here
\hyphenation{}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Fast Fourier Transform on the Versat CGRA}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
\author{\IEEEauthorblockN{Jo\~ao D. Lopes}
\IEEEauthorblockA{INESC-ID Lisboa / T\'ecnico Lisboa\\
University of Lisbon\\
Email: joao.d.lopes@ist.utl.pt}
\and
\IEEEauthorblockN{Jos\'e T. de Sousa}
\IEEEauthorblockA{INESC-ID Lisboa / T\'ecnico Lisboa\\
University of Lisbon\\
Email: jts@inesc-id.pt}}

% make the title area
\maketitle


\begin{abstract}
%\boldmath
In this paper we present a Fast Fourier Transform (FFT) algorithm for
the Versat architecture, a small and low power Coarse Grained
Reconfigurable Array (CGRA). This approach targets ultra low energy
applications such as those found in Wireless Sensor Networks (WSNs),
where using a GPU or FPGA accelerator is out of the question. The
implementation chosen was the Radix-2 FFT, and uses 32-bit integer
precision in the Q1.31 format. The Versat program creates two base
hardware datapaths for the two basic steps of the algorithm: the
complex multiplication and the complex addition steps. These datapaths
are partially reconfigured during execution. The program has been
written in assembly and has 873 instructions. It is fully
parameterizable with the number of datapoints, FFT window size,
overlap size and memory pointers for reading and writing back the data
to the external memory. The results show that the new Versat core is
9.4x smaller than an ARM Cortex A9 core, runs the algorithm on average
18x faster and consumes 219.51x less energy.

\end{abstract}

\begin{IEEEkeywords}
  FFT, reconfigurable computing, coarse-grained reconfigurable arrays
\end{IEEEkeywords}

\IEEEpeerreviewmaketitle


\section{Introduction}

The Fast Fourier Transform (FFT) is an algorithm for efficiently
computing the Discrete Fourier Transform (DFT) of a digitized signal,
and very useful to perform spectral analysis. It is used in many
signal processing applications such as filters, audio and video pre
and post processing as well as encoders and decoders. The DFT converts
the data from its original domain (often time or space) into a
representation in the frequency domain. The FFT algorithm became
popular in 1965, although its principles had been developed back in
1805~\cite{heideman1984gauss}. After Gilbert Strang said that it is
''the most important numerical algorithm of our
lifetime''~\cite{strang1994wavelets}, in 1994 it was added to Top 10
Algorithms of the 20th Century by the IEEE Computing in Science \&
Engineering journal~\cite{dongarra2000guest}. One of the most widely
used FFT algorithms is the Cooley-Tukey algorithm, addressed in this
paper.

Three relevant examples of low energy FFT applications are (1)
wireless comunications~\cite{garzia2009crema}, (2) sensor biosignal
data processing~\cite{patel2011syscore} and (3) Wireless Sensor
Networks (WSNs)~\cite{canli2006power,xu2012power}. Since there is an
increasing scientific interest in Big Data (i.e., very large
datasets), real-time data processing emphasize the needed for fast FFT
computation. This problem can be attenuated by the use of hardware
accelerators.

The FFT algorithm has been accelerated using Graphics Processor Units
(GPUs)~\cite{moreland2003fft,doggettfft,fialka2006fft} and using Field
Programmable Gate Arrays
(FPGAs)~\cite{chao2005design,sun2008design,derafshi2010high}. Although
these implementations have been shown to improve performance and
energy consumption over General Purpose Processors (GPPs)
implementations, they can hardly be used in ultra low energy
scenarios, such as WSNs. Local processing of sensor data allows
reducing the data that needs to be transmitted. Thus, it is more
efficient, in terms of time and energy, to run the FFT algorithm in a
distributed fashion rather than in a centralized way. Since WSN nodes
are extremely constrained in terms of compute power and energy
consumption, a smaller and more power efficient accelerator should be
used, such as a Coarse Grained Reconfigurable Array (CGRA). A
dedicated hardware accelerator may also be used but its lack of
programmability can become a serious liability in the long run.

This paper presents an in-depth study of a fully parameterizable
implementation of the FFT algorithm for a previously published CGRA --
the Versat architecture~\cite{Lopes16}. The Versat CGRA features a
relatively low number of functional units, achieving a very small area
footprint and low energy consumption. Reconfiguration is quasi-static
(occurring only after complete loop nests are run) and partial (only
the configuration words that differ from the previous configuration
are changed). Unlike other CGRAs, which are programmed by
pre-compiling configurations for their programmable hardware, Versat
uses a very small 16-instruction controller, which generates and/or
modifies hardware configurations at runtime. The Versat controller can
be programmed in assembly language or using a small C++ subset for
which a compiler has been described in~\cite{Santiago2017}. The FFT
software for Versat has been written in assembly.

The proposed algorithm computes the FFT on a sliding window over a
long discrete signal. It makes use of Data-Level Parallelism (DLP) and
Instruction-Level Parallelism (ILP). As is demonstrated in this paper,
the algorithm is highly scalable with the window size and signal
length, with increasing acceleration compared to GPP
implementations. The data format is 32-bit fixed-point in a Q1.31
organization. The Versat architecture does not support floating point
data, yet.

The remainder of the paper is organized as
follows. Section~\ref{sec:fft} describes the FFT
algorithm. Section~\ref{sec:versat} briefly outlines the Versat
architecture. Section~\ref{sec:kernel} presents the proposed
algorithm. Section~\ref{sec:results} presents implementation and
performance results. Section~\ref{sec:conc} concludes the paper.


\section{FFT Theory}
\label{sec:fft}

The Fast Fourier Transform (FFT) is an expedite way to compute the
Discrete Fourier Transform of a discrete time signal. There are many
implementations of the FFT algorithm. For this work the Radix-2 FFT
algorithm has been chosen, due to its simplicity. In the next section,
a formal description of the DFT is presented, and in the section
after, the Cooley-Tukey Radix-2 FFT algorithm is described.

\subsection{Discrete Fourier Transform}
\label{sec:FFTAlgorithm}

The Discrete Fourier Transform $X(k)$ of a discrete signal $x(n)$
having $N$ samples is given by

\begin{equation}
X(k)=\sum_{n=0}^{N-1} W_N^{kn}x(n),\ \ 0\leq k\leq N-1,
\label{eq:DFT}
\end{equation}
where the coefficients $W_N^k$ are given by
\begin{equation}
W_N^k=e^{-j\frac{2\pi k}{N}}.
\label{eq:DFTcoefficients}
\end{equation}

The most obvious way to compute a DFT point $X(k)$ is to perform $N$
complex multiplications ($4N$ real multiplications) and $N-1$ complex
additions ($2N-2$ real additions). Because one needs to compute $N$
points of the DFT, the algorithm complexity is $O(N^2)$. Attending to
the coefficients symmetry (equation~\ref{eq:coeffSymmetry}) and
periodicity (equation~\ref{eq:coeffPeriod}), it is possible achieve a
better algorithm, and the Cooley-Tukey FFT Algorithm, explained in the
next section, is just that.

\begin{equation}
W_{N}^{k+\frac{N}{2}}=-W_{N}^{k}
\label{eq:coeffSymmetry}
\end{equation}

\begin{equation}
W_{N}^{k+N}=W_{N}^{k}
\label{eq:coeffPeriod}
\end{equation}


% ----------------------------------------------------------------------
\subsection{Cooley-Tukey FFT Algorithm}
\label{sec:CooleyTukeyAlgorithm}

The DFT computation of $N$ points can be decomposed in two independent
DFT computations of $N/2$ points, for $N$ even, separating the points
which have even indices from the ones that have odd
indices. Equation~(\ref{eq:DFT}) can thus be re-written as

\begin{equation} 
  X(k) = \sum_{n=0}^{(N/2)-1} W_N^{k(2n)} x(2n) + 
  \sum_{n=0}^{(N/2)-1} W_N^{k(2n+1)} x(2n+1).
  \label{eq:splitDFT}
\end{equation}

From equation~(\ref{eq:DFTcoefficients}) it can be derived that
$W_N^{2k} = W_{N/2}^k$, and equation~(\ref{eq:splitDFT}) can be re-written as
\begin{equation}
X(k)=\sum_{n=0}^{(N/2)-1} W_{N/2}^{kn} f_e(n) + W_N^k \sum_{n=0}^{(N/2)-1} W_{N/2}^{kn} f_o(n),
\label{eq:splitDFT2}
\end{equation}
where $f_e(n) = x(2n)$ and $f_o(n)= x(2n+1)$. Hence,
equation~(\ref{eq:splitDFT2}) can be written as

\begin{equation} 
X(k)=F_e(k)+W_N^k F_o(k), \ \ 0\leq k\leq N-1.
\label{eq:DFTdecomp}
\end{equation}
\newline

Since $F_e(k)$ and $F_o(k)$ are periodic with period $N/2$, and taking
into account the coefficients' symmetry, according to
equations~(\ref{eq:coeffSymmetry}) and~(\ref{eq:coeffPeriod}), the
previous expression can be re-written as

\begin{multline}
\begin{split}
X(k) & =  F_e(k)+W_n^k F_o(k), \\
X \left ( k+\frac{N}{2} \right ) & =  F_e(k)-W_n^k F_o(k), \ \ 0\leq k\leq \frac{N}{2}-1.
\end{split}
\label{eq:DFTdecomp2}
\end{multline}

Equation~(\ref{eq:DFTdecomp2}) proves that it is possible to decompose
an $N$-point DFT into a weighted sum of {\em two} $N/2$-point
DFTs. This procedure can be applied recursively until the simplest
case of a 2-point DFT is reached. This happens after $log_2(N)$
recursion levels, which explains why this FFT algorithm is called {\em
  Radix-2}. In this algorithm there are $(N/2)log_2(N)$ complex
multiplications and $Nlog_2(N)$ complex additions. Therefore, the
Cooley-Tukey algorithm has complexity $O(Nlog(N))$.

A 2-point FFT computed in this way is shown in
figure~\ref{fig:FFTButterfly}. This is called a butterfly diagram
because of its shape. The input data is on the left, while the output
data is on the right. All data are complex numbers. The edges
represent values which may be weigthed by the coefficients $W_N$ and
$-1$. Where edges merge, their corresponding values are added. For
optimization, an edge having the coefficient $-1$ is simply subtracted
from the other merging edge.

\begin{figure}[!htb]
	\centering{
      \includegraphics
            [width=0.5\columnwidth]
            {drawings/fftButterfly}
	}	
	\caption{Butterfly diagram.}
	\label{fig:FFTButterfly}
\end{figure}

A graphical example of the Cooley-Tukey algorithm applied to an
8-point signal $x$ is shown in figure~\ref{fig:FFT8pts}. The recursion
levels map to stages. In this example, there are $log_2(8)=3$
stages. Each stage have distinctive groups of butterflies called
blocks. The first stage has 4 blocks of a single butterfly, the second
stage has 2 blocks of 2 butterflies and the third stage has a single
block of 4 butterflies.

\begin{figure}[!htb]
	\centering{
      \includegraphics
            [width=\columnwidth]
            {drawings/fft8pts}
	}	
	\caption{Cooley-Tukey agorithm appled to an 8-point signal $x$}
	\label{fig:FFT8pts}
\end{figure}

The Versat FFT kernel implements the Cooley-Tukey algorithm by
splitting a basic butterfly structure in two steps:
\begin{enumerate}
	\item Complex multiplication step: the datapoints stored
          at odd addresses are multiplied by the coefficients
	\item Complex addition step: the datapoints stored at even
          addresses are added and subtracted from the results of the
          first step to yield 2 result datapoints
\end{enumerate}

\section{Versat Architecture}
\label{sec:versat}

The Versat architecture is presented in detail
in~\cite{Lopes16}. Here, only a brief description is presented for
self containability reasons. The top level entity is shown in
figure~\ref{fig:top}. Referring to this figure, the Controller
executes the program stored in the 2048x32bit Program Memory (PM). The
communication between a host system and Versat is done through the
Control Register File (CRF), consisting of 16 registers, R0 through
R15. The host uses the CRF to pass parameters to Versat programs,
initiate their execution and check if they have finished. The Data
Engine (DE) is where most computations take place. The controller
writes new DE configurations to the Configuration Module (CM). These
configurations can be selected for execution and/or partially
modified. The Direct Memory Access (DMA) module is used to access the
external memory and transfer data/instructions/configurations to/from
the DE/PM/CM, respectively. The DE receives commands from the
controller and informs the controller of its status. The DE is run
many times during the execution of a program, for many different DE
configurations.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{top.png}
  \caption{Versat top-level entity.}
  \label{fig:top}
\end{figure}

\subsection{Controller}
\label{sec:controller}

The Versat controller runs the kernel algorithms and is responsible
for the overall control of the system, especially the reconfiguration
process. It performs loads and stores (from/to the Control Bus), a few
arithmetic and logic operations, and branch instructions. The
controller has not been designed for high performance. Its instruction
set consists of just 16 instructions and its performance is just
enough for controlling the main steps of the algorithms, reconfiguring
the DE and operating the DMA; data intensive computations are done in
the DE.

\subsection{Data Engine}
\label{sec:de}

The Data Engine (DE) is a 32-bit architecture and comprises 15
functional units (FUs) as shown in figure~\ref{fig:de}. There are four
2048x32bit dual port embedded memories, six ALUs, four multipliers and
one barrel shifter. The outputs of all FUs are concatenated to form a
19x32-bit Data Bus. (Since each memory contributes two outputs, there
are {\bf 2}*4+6+4+1=19 bus sections.) Each FU input can be configured
to select any of the 19 sections of the bus -- this implements a full
mesh topology and avoids contention on the Data Bus.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{de.png}
  \caption{Versat data engine.}  
  \label{fig:de}
\end{figure}

The embedded memories have one Address Generator Unit (AGU) for each
port. The AGU can support two-level {\tt for} loop nests. It is
possible to configure and run each AGU independently. The AGU
parameters are described in table~\ref{tab:MemParameter}.

\begin{table}[h!]
    \caption{Address generator unit parameters.}
  \begin{center}
    \begin{tabular}{|l|c|p{5.5 cm}|}
      \hline
      {\bf Parameter} & {\bf Size} (bits) & {\bf Description} \\
      \hline \hline
      Start & 11 & Memory start address. Default value is 0. \\
      \hline
      Per  & 5 & Number of iterations of the inner loop, aka Period. Default is 1 (no inner loop). \\
      \hline
      Duty & 5 & Number of cycles in a period (Per) that the memory is enabled. Default is 1.\\
      \hline
      Incr & 11 & Increment for the inner loop. Default is 0.\\
      \hline
      Iter & 11 & Number of iterations of the outer loop. Default is 1.\\
      \hline
      Shift & 11 & Additional increment in the end of each period. Note that Per+Shift is the increment of the outer loop. Default is 0.\\
      \hline
      Delay & 5 & Number of clock cycles that the AGU must wait before starting to work. Used to compensate different latencies in the converging branches of the configured hardware datapaths. Default is 0.\\
      \hline
      Reverse & 1 & Bit-wise reversion of the generated address. Certain applications like the FFT work with mirrored addresses. Default is 0.\\
      \hline
    \end{tabular}
  \end{center}
  \label{tab:MemParameter}
\end{table}

By configuration, the AGU can be bypassed and the memory port address
is input by the other port. What this actually does is to provide
Versat with the capability of working with pointers, as these can be
computed in the DE and routed to any embedded memory to be used as
addresses.  In addition, the number sequences generated by an AGU can
be output to the DE for general purposes, instead of being used as
addresses. This exploits the ability of the AGUs to generate
synchronization sequences, making it possible to use them to
synchronize FUs in the DE. The two ports can be independently
configured to read and/or write the memories. The write configuration
includes the selection of a data bus section to be written in the
memory.

There are two types of ALUs, which differ in the supported
operations. Two ALUs are of type I, supporting basic arithmetic and
logic operations, while four ALUs are of type II, supporting basic
operations plus the same operations where one operand is internally
connected to the ALU output itself, freeing the respective input to be
used for conditional control. This implements internal feedback and
conditional execution to type II ALUs. For example, the basic
operation of {\em addition} can be turned into a conditional
accumulate operation: the ALU can register or accumulate the value
present at one of its inputs, depending on the value present at the
other input. In another example, the {\em minimum} operation can be
used to detect and register the minimum value of a sequence present at
one of the ALU inputs, while the control input qualifies the value
present at the data input. For example, if an AGU is used to generate
a control sequence for the conditional ALU input, the computation of
the minimum and registration of the result at the ALU output will
happen only in certain time instants; at other instants the last
computed minimum is kept.

The multipliers take two 32-bit operands and produce a 64-bit result,
of which only 32 bits are used. It is possible to choose the upper or
lower 32-bit part of the result. It is also possible multiply the
output by two, which is useful to keep a Q1.31 fixed-point format used
in many DSP algorithms.

For the barrel shifter, one operand is the word to be shifted and the
other operand is the shift size. The barrel shifter is configured with
the shift direction (left or right) and the right shift type
(arithmetic or logic).

Each FU has a latency due to pipelining: 2 cycles for the ALU, 3
cycles for the multiplier and 1 cycle for the barrel shifter. Thus,
when configuring a datapath in the DE, it is necessary to take into
account the latency of each datapath branch, and compensate for any
mismatches when branches with different latencies converge. To do
this, the AGUs have the {\em Delay} parameter explained in
table~\ref{tab:MemParameter}. The branch latency is the sum of its FU
latencies.

To control the DE, its control and status registers are used. It is
possible to initialize/run the FUs by setting bits 0/1 of the control
register, respectively. Bits 2-20 select the FUs to initialize or
run. Since there are 11 operation FUs and 4 memory FUs and each of the
2 memory ports can be controlled independently, there are 11+2x4=19
bits in the DE control register for selecting the FUs. To check the
DE, its status register is used. The status register indicates which
AGUs have ended execution (bits~0~to~7).


\subsection{Configuration Module}

The configurations of the DE are stored in the Configuration Module
(CM). The CM comprises the configuration register file, the
configuration shadow register and the configuration memory. The
configuration register file is used by the Versat controller to write
configuration words to the DE (partial reconfiguration). The shadow
register allows keeping the currently active DE configuration
while a future configuration is being created in the configuration
register. The configuration memory can be used to save 64 frequently
used DE configurations. A configuration in the configuration register
file can be saved in the configuration memory in 1 clock cycle and
reloaded later for reuse also in 1 clock cycle.

\subsection{DMA}
\label{sec:DMAengine}

The Versat controller uses the DMA module to access an external memory
and transfer programs, data and configurations. Though Versat can load
configurations from the external memory, this feature has never been
used as Versat is designed to generate its own configurations and does
not in principle need to access configurations from the external
memory. The maximum block transfer size is 256 words of 32 bits. The
DMA can work in parallel with the controller and the DE.


\section{The FFT Kernel}
\label{sec:kernel}

The FFT kernel follows the algorithm given in section~\ref{sec:fft},
creating and partially reconfiguring two basic hardware datapaths that
realize the {\em complex multiplication} and {\em complex addition}
steps of the algorithm. In fact, two versions of each datapath have
been created to enable ping-pong data processing. Another datapath is
used to initially reorder the datapoints by reverting the bits of
their addresses. The kernel is written in the Versat assembly language
and it is 873 instructions long.

The datapath to initially reorder the data is shown in
figure~\ref{fig:mirrorAddresses}). The address generators of the
source memories MEM2 and MEM3 read the datapoints with the Reverse
parameter set to 1 (table~\ref{tab:MemParameter}), and the address
generators of the destination memories MEM0 and MEM1 write them
sequentially. Concurrently, the table of coefficients is copied from
MEM2 to MEM0, so that either can be accessed during ping-pong
processing.

\begin{figure}[!htb]
\centering
\includegraphics[width=\columnwidth]{drawings/fft-mirrorAddresses}
\caption{Datapath for read the initial datapoints with mirrored
  addresses.}
\label{fig:mirrorAddresses}
\end{figure}

The Versat controller plays an important role in complex kernels like
the present FFT kernel. It is responsible for generating the
datapaths, reconfiguring them and controlling the algorithm. Namely,
it controls the outer loops over all stages and, in some cases, over
all blocks in a stage.

First, the four datapaths are created and stored in the configuration
memory. They are very similar two by two, swapping only the memories
that are used for reading with the ones that are used for storing the
data. This is done while data is being DMA transferred into the DE.

After reading a number of parameters from the CRF (number of
datapoints, window and overlap sizes and addresses to read/write data
in the external memory), as passed by the host, the program instructs
the DMA to load the first datapoint chunk values in memories MEM2 and
MEM3. Real parts go for MEM2 and imaginary parts for MEM3, not
exceeding the 1024 lower addresses. Coefficient values are also DMA
transferred into memory MEM2, occupying at most the 1024 higher
addresses. Then the data is reordered and placed in memories MEM0 and
MEM1 while the coefficients are copied to MEM0, using the reordering
datapath explained before.

The datapath in figure~\ref{fig:complexMultiplication} implements the
complex multiplication step, reading the data from MEM0 (real part)
and MEM1 (imaginary part) and coefficients from MEM2 (real and
imaginary parts), and writing the result back in MEM0 and MEM1. Its
configuration is loaded from the configuration memory into the
configuration register in one clock cycle, partially reconfigured, and
shifted to the shadow configuration register in another cycle. The
datapath forms a read-multiply-add-write pipeline with four
multiplications and two additions in parallel in the respective
stages, combining DLP and ILP.

\begin{figure}[!htb]
\centering \includegraphics[width=\columnwidth]{drawings/fft-cmult}
\caption{Datapath for the complex multiplication step.}
\label{fig:complexMultiplication}
\end{figure}

Next, the datapath that performs the complex addition step, shown in
figure~\ref{fig:complexSum}, is loaded. This datapath performs the two
complex additions in the butterfly diagram of
figure~\ref{fig:FFTButterfly}. Note that MEM0 (real part) and MEM1
(imaginary part) have the original data in the even addresses and the
original data multiplied by the coefficients in the odd
addresses. Therefore the data in the even addresses is added and
subtracted to the data in the odd addresses to produce the two
butterfly outputs. The results are placed in MEM2 (real part) and MEM3
(imaginary part). The parallel additions exploit DLP while the
read-compute-write pipeline exploits ILP.

\begin{figure}[!htb]
\centering \includegraphics[width=\columnwidth]{drawings/fft-csum}
\caption{Datapath for the complex addition step.}
\label{fig:complexSum}
\end{figure}

After both steps are run, the controller increments the outer loop and
the two steps are applied again, now from MEM2 and MEM3 to MEM0 and
MEM1. This process is repeated until all stages are processed. Every
time the kernel needs more data, the current results are stored in the
external memory and new datapoints are loaded into the DE memories.

Because AGU parameter Period has only 5 bits
(table~\ref{tab:MemParameter}), there is need to break the two step
computations in several parts for some stages. This is because the two
nested loops that can be executed in the DE are used to go over all
blocks and all datapoints within a block. If there are more than
$2^5=32$ points within a block then the inner loop cannot be
used. Only the iterations loop is used and the DE needs to be
reconfigured for each block. In order to save reconfiguration time,
the controller uses partial reconfiguration. Additionally, if an FFT
window cannot fit into the DE memories, the FFT computation is divided
into several smaller FFTs and several other steps to merge the
results.

After all stages are processed, the controller instructs the DMA to
transfer the results to the external memory, using the respective
address in the CRF passed by the host. Then, a new datapoint chunk is
loaded and the whole process starts again, until all datapoints are
processed and stored in the external memory. When this happens, the
algorithm terminates. Before returning to the boot ROM memory, the
controller clears register R0 in the CRF, which signals the host that
Versat has finished execution.

\section{Results}
\label{sec:results}

In this section, design and performance results are presented. Versat
has been designed as an IP core, which can be integrated in a System
on Chip (SoC). Results on core area, frequency and power are
given. Versat's performance for the present FFT algorithm is measured
and compared to a widely used embedded processor: the ARM Cortex A9
processor.

\subsection{IP core Implementation Results}

Versat has been designed using a UMC 130nm process and its main
features are shown in table~\ref{tab_asic_r}: technology node (N),
silicon area (A), embedded memory (RAM), frequency of operation (F)
and power (P). For the sake of comparison we also show the features of
a previous Versat version and of the ARM Cortex A9 processor,
implemented in a 40nm technology and optimized for
power~\cite{wang}.

The Versat frequency and power results for the 130nm process have been
obtained using the Cadence IC design and simulation tools. The
frequency is reported by the place and route tool, which produces a
netlist with back-annotated layout information. Power is obtained by
simulating this netlist to get node activities, and then computing a
power figure. For synthesis, the Encounter RTL Compiler tool (v11.20)
has been used. For place and route, the Encounter Design
Implementation tool (v11.10) has been used. For simulation, the NCSIM
simulator (v12.10) has been used. To better compare with the ARM
processor, the last row in table~\ref{tab_asic_r} shows the Versat
features scaled to a 40nm technology. These results have been obtained
by applying scaling rules as explained in~\cite{borkar99}.

\begin{table}[h]
\caption{Integrated circuit implementation results}
\label{tab_asic_r}
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Core & N(nm) & A(mm\textsuperscript{2}) & RAM(kB) &  F(MHz) & P(mW)\\
\hline
\hline
Versat~\cite{Lopes16} & 130 & 4.2  &  46.34 & 170 &  99 \\
\hline
Versat~[here] & 130 & 5.2  &  46.34 & 170 &  132 \\
\hline
ARM Cortex A9~\cite{wang} & 40 & 4.6 &  65.54 & 800 &  500 \\
\hline
Versat [here, scaled] & 40 & 0.49  &  46.34 & 553 &  41 \\
\hline
\end{tabular}
\end{table}

From the above results, we conclude that the current Versat core is
9.4x smaller than the ARM core and consumes 12.2x less power, assuming
both cores are clocked at their work frequencies. The Versat power has
been obtained by simulating the present FFT algorithm. The results
show that the current Versat version is 24\% larger than the previous
version presented in~\cite{Lopes16}, and consumes 32\% more power.
The ARM power figure is an average value reported in~\cite{wang}.

\subsection{Performance Results}

To study our solution's performance we successively computed the FFT on
a sliding window of a certain size over a large sequence of datapoints
randomly generated. Two consecutive windows may overlap for a number
of datapoints, called the overlap size. The experiments consider
varying the number of data points, window and overlap size. We
measured the execution time for both the Versat and the ARM Cortex A9
systems, which is given by the number of clock cycles divided by the
clock frequency in the 40nm process SoC, according to
table~\ref{tab_asic_r}.

The Xilinx Zynq 7010 platform has been used to measure the performance
of both the Versat core and the ARM Cortex A9 core, available in the
Zynq platform as a hard macro. The ARM results have been obtained
using a system timer in a bare metal implementation of the FFT
algorithm. The Versat core has been added as a peripheral of the ARM
core, controlled from its AXI slave interface and accessing the
external memory via its AXI master interface. The ARM core has been
used to measure the Versat execution time using the same system timer.

In figure~\ref{fig:timeVSdpts}, we show the execution time as a
function of the number of datapoints in a static window, i.e., no
sliding occurs. These results show that, for both the Versat and ARM
cores, the execution time scales according to the algorithm's
complexity ($Nlog_2(N)$).

\begin{figure}[!htb]
  \centering \includegraphics[width=3.5in]{Npts.png}
  \caption{Execution time vs. FFT static window size.}
  \label{fig:timeVSdpts}
\end{figure}

The execution time of both cores is affected by the size of their
internal memories. In the Versat case it is the size of its embedded
memories and in the ARM case it is its cache system, L1 plus L2. This
phenomenon is visible in figure~\ref{fig:speedup1}, where the speedup
versus the number of datapoints is shown. The speedup $SU$ is computed
by
\begin{equation*}
SU = \frac{N_A f_v}{N_v f_A},
\end{equation*}
where $N_A$ and $N_V$ are the number of execution clock cycles for the
ARM and Versat cores, respectively, and $f_A$ and $f_V$ are the
operation frequencies for the ARM and Versat cores, respectively.
After the FFT window size reaches the capacity of the Versat memories,
the speedup declines and starts increasing again when it reaches the
ARM's L1 cache size. For 16k points the speedup is more than 17. This
shows that Versat can manage concurrent data transfers from external
memory better than the ARM core.

\begin{figure}[!htb]
  \centering \includegraphics[width=3.5in]{speedup1.png}
  \caption{Speedup vs. FFT static window size.}
  \label{fig:speedup1}
\end{figure}

In figure~\ref{fig:timeVSoverlap}, we show the execution time as a
function of the overlap size for a 1024-point sliding window. For this
experiment, the execution time grows much faster for the ARM core
compared to the Versat core.

\begin{figure}[!htb]
  \centering \includegraphics[width=3.5in]{NptsOverlap.png}
  \caption{Execution time vs. overlap size for a 1024-point sliding
    window.}
  \label{fig:timeVSoverlap}
\end{figure}

The speedup is higher than in the previous experiment and is linear in
the overlap size, according to figure~\ref{fig:speedup2}. This is
because Versat always keeps the overlapped points in its memories for
the next FFT window, transferring only the remaining points. Instead,
the ARM system uses its cache and data prefetch mechanisms for
managing data streaming automatically, which proves to be a worse
solution.

\begin{figure}[!htb]
  \centering \includegraphics[width=3.5in]{speedup2.png}
  \caption{Speedup vs. overlap size for a 1024-point sliding window.}
  \label{fig:speedup2}
\end{figure}

In figure~\ref{fig:windowSize}, we show the execution time as a
function of the sliding window size. Initially the execution time
raises rapidly as the size of the internal memories is reached. After
that the increase rate slows down.

\begin{figure}[!htb]
  \centering \includegraphics[width=3.5in]{windowSize.png}
  \caption{Execution time vs. sliding window size for half window
    overlap.}
  \label{fig:windowSize}
\end{figure}

The speedup as a function of the sliding window size is illustrated by
figure~\ref{fig:speedup3}. As the sliding window reaches the capacity
of the Versat memories, the speedup drops, while the ARM caches and
prefetch mechanisms continue to work well. However, as the window size
increases, the ARM core reaches its own internal memory limitations
for streaming data, and the speedup increases steadily after that.

\begin{figure}[!htb]
  \centering \includegraphics[width=3.5in]{speedup3.png}
  \caption{Execution time vs. sliding window size for half window
    overlap.}
  \label{fig:speedup3}
\end{figure}

The energy ratio $ER$ of using Versat compared to using the ARM Cortex
A9 processor, that is, the ratio of the energy consumed by the ARM
core and the energy consumed by the Versat core to execute the FFT
algorithm, can be computed by
\begin{equation*}
  ER = SU \frac{P_A}{P_V} = 219.51,
\end{equation*}
where $SU=18$ is the average speedup, and $P_A$ and $P_V$ are the
power consumption of the ARM and Versat cores, respectively, as given
in table~\ref{tab_asic_r} for the 40nm process.


\section{Conclusion}
\label{sec:conc}

In this paper, an in-depth study of an FFT algorithm targeting the Versat
CGRA has been presented. Although there are quite a few solutions for
accelerating this algorithm in GPUs and FPGAs, these are not good
solutions for high performance / ultra low energy embedded
applications. For example, the FFT algorithm is useful in wireless
sensor network nodes, which are severely constrained in terms of
compute capabilities and energy consumption.

The Versat architecture has limited compute resources but it is
extremely flexible to program. In fact, Versat can be programmed like
a traditional controller, where the program itself creates and
partially reconfigures hardware datapaths in its data engine. The full
mesh topology of the data engine is easy to deal with by
programmers. Versat is capable of useful acceleration at low clock
rates and, given its small size, it can save orders of magnitude in
energy. It is designed to be a programmable alternative to dedicated
hardware accelerators, eliminating the risk of design errors. This
paper briefly outlines the Versat architecture.

The proposed FFT algorithm comprises two main steps: the complex
multiplication and the complex addition steps. The algorithm uses a
basic hardware datapath for each of these steps. The complex
multiplication datapath multiplies the datapoints with odd addresses
by the FFT coefficients. The complex addition datapath adds the
complex multiplication result and the odd address datapoints, and
subtracts the complex multiplication result from even address
datapoints. These results are stored in the Versat embedded memories
to be used in the next FFT stage. In order to improve performance, the
basic datapaths are runtime reconfigured to swap origin and
destination memories in a ping-pong fashion, and to change the
addresses for accessing data from the memories.

Our results show that Versat is 9.4x smaller than an ARM Cortex A9
processor, and runs the FFT algorithm on average 18x faster, while
consuming 219.51x less energy. It should be clear from these numbers
that GPUs and FPGAs cannot compete in this arena, and that the
presented solution is useful in applications where cost and energy
consumption are crucial. \\

% use section* for acknowledgement
\section*{Acknowledgment}

This work was supported by national funds through Funda\c c\~ao para a
Ci\^encia e a Tecnologia (FCT) with reference UID/CEC/50021/2013.

\bibliographystyle{unsrt}
\bibliography{BIBfile}

% that's all folks
\end{document}


